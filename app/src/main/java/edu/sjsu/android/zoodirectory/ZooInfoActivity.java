package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class ZooInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zooinfo_activity);
        Button numberDial = (Button) findViewById(R.id.dial_number);
        TextView Zooinfo = (TextView) findViewById(R.id.zooname);

        Zooinfo.setText("ZZ's Zoo");

        numberDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = "tel: +8888888";
                Intent callActivity = new Intent(Intent.ACTION_DIAL, Uri.parse(number));
                startActivity(callActivity);
            }
        });
    }
}
