package edu.sjsu.android.zoodirectory;

public class Animal {
    private String name;
    private int image;
    private String description;
    private int imageLarge;

    Animal(String name, int image, String description, int imageLarge){
        this.name = name;
        this.image = image;
        this.description = description;
        this.imageLarge = imageLarge;
    }

    String getName(){
        return name;
    }

    int getImage(){
        return image;
    }

    String getDescription(){
        return description;
    }

    int getImageLarge() {return imageLarge;}
}
