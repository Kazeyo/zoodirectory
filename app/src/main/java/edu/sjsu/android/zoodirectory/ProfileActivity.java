package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_active);
        TextView nameText = (TextView) findViewById(R.id.nameText);
        TextView desText = (TextView) findViewById(R.id.desTextview);
        ImageView largePics = (ImageView) findViewById(R.id.AnimalLargePics);

        String name = "Username not set";
        String description = "null description";

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            name = extras.getString("name");
            description = extras.getString("description");
            largePics.setImageResource(extras.getInt("Image"));
        }
        nameText.setText(name);
        desText.setText(description);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.zoo_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.infoBar:
                Intent myIntent = new Intent(getApplicationContext(), ZooInfoActivity.class);
                startActivity(myIntent);
                return true;
            case R.id.uninstallbar:
                Uri packageUri = Uri.parse("package:edu.sjsu.android.zoodirectory");
                Intent delete = new Intent(Intent.ACTION_DELETE, packageUri);
                startActivity(delete);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
