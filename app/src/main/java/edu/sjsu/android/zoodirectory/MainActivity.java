package edu.sjsu.android.zoodirectory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    Animal giraffe = new Animal("giraffe", R.drawable.giraffe, "The giraffe (Giraffa) is an African artiodactyl mammal, the tallest living terrestrial animal and the largest ruminant. It is traditionally considered to be one species, Giraffa camelopardalis, with nine subspecies. However, the existence of up to nine extant giraffe species has been described, based upon research into the mitochondrial and nuclear DNA, as well as morphological measurements of Giraffa. Seven other prehistoric species, known from fossils, are extinct.", R.drawable.giraffe_large);
    Animal lemur = new Animal("lemur", R.drawable.lemur, "Lemurs (/ˈliːmər/ (About this soundlisten) LEE-mər) (from Latin lemures – ghosts or spirits) are wet-nosed primates of the superfamily Lemuroidea, divided into 8 families and consisting of 15 genera and around 100 existing species. They are native only to the island of Madagascar. Most existing lemurs are small, have a pointed snout, large eyes, and a long tail. They chiefly live in trees (arboreal), and are active at night (nocturnal).", R.drawable.lemur_large);
    Animal monkey = new Animal("monkey", R.drawable.monkey, "Monkey is a common name that may refer to most mammals of the infraorder Simiiformes, also known as the simians. Traditionally, all animals in the group now known as simians are counted as monkeys except the apes, a grouping known as paraphyletic; however in the broader sense based on cladistics, apes (Hominoidea) are also included, making the terms monkeys and simians synonyms in regard of their scope. Monkeys are divided into the families of New World monkeys (Platyrrhini) and Old World monkeys (Cercopithecidae in the strict sense; Catarrhini in the broad sense, which again includes apes).", R.drawable.monkey_large);
    Animal panda = new Animal("panda", R.drawable.panda, "The giant panda (Ailuropoda melanoleuca; Chinese: 大熊猫; pinyin: dàxióngmāo),[5] also known as the panda bear (or simply the panda), is a bear[6] native to South Central China.[1] It is characterised by its bold black-and-white coat and rotund body. The name \"giant panda\" is sometimes used to distinguish it from the red panda, a neighboring musteloid. Though it belongs to the order Carnivora, the giant panda is a folivore, with bamboo shoots and leaves making up more than 99% of its diet.[7] Giant pandas in the wild will occasionally eat other grasses, wild tubers, or even meat in the form of birds, rodents, or carrion. In captivity, they may receive honey, eggs, fish, yams, shrub leaves, oranges, or bananas along with specially prepared food.[8][9]", R.drawable.panda_large);
    Animal bird = new Animal("bird", R.drawable.bird, "Birds are a group of warm-blooded vertebrates constituting the class Aves /ˈeɪviːz/, characterised by feathers, toothless beaked jaws, the laying of hard-shelled eggs, a high metabolic rate, a four-chambered heart, and a strong yet lightweight skeleton. Birds live worldwide and range in size from the 5.5 cm (2.2 in) bee hummingbird to the 2.8 m (9 ft 2 in) ostrich. There are about ten thousand living species, more than half of which are passerine, or \"perching\" birds. Birds have wings whose development varies according to species; the only known groups without wings are the extinct moa and elephant birds. Wings, which evolved from forelimbs, gave birds the ability to fly, although further evolution has led to the loss of flight in some birds, including ratites, penguins, and diverse endemic island species. The digestive and respiratory systems of birds are also uniquely adapted for flight. Some bird species of aquatic environments, particularly seabirds and some waterbirds, have further evolved for swimming.", R.drawable.bird_large);

    List<Animal> animals = new ArrayList<>();

    private MyAdaptwer.RecyclerViewClickListener listener;

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        animals.add(giraffe);
        animals.add(lemur);
        animals.add(monkey);
        animals.add(panda);
        animals.add(bird);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.my_recycler_view);

        setOnClickListner();
        MyAdaptwer mAdapter = new MyAdaptwer(animals, listener);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setOnClickListner() {
        listener = new MyAdaptwer.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                if(position == animals.size() - 1){
                    MyQuestionDialogFragment fragment = new MyQuestionDialogFragment();
                    fragment.show(getSupportFragmentManager(), "question");
                }
                else{
                    Intent myIntent = new Intent(MainActivity.this, ProfileActivity.class);
                    myIntent.putExtra("name", animals.get(position).getName());
                    myIntent.putExtra("description", animals.get(position).getDescription());
                    myIntent.putExtra("Image", animals.get(position).getImageLarge());
                    startActivity(myIntent);
                }
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.zoo_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.infoBar:
                Intent myIntent = new Intent(context, ZooInfoActivity.class);
                startActivity(myIntent);
                return true;
            case R.id.uninstallbar:
                Uri packageUri = Uri.parse("package:edu.sjsu.android.zoodirectory");
                Intent delete = new Intent(Intent.ACTION_DELETE, packageUri);
                startActivity(delete);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyQuestionDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedIstanceState){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("the animal is very scary, do you want to proceed?").setPositiveButton("yes",
            new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent myIntent = new Intent(getContext(), ProfileActivity.class);
                    myIntent.putExtra("name", "bird");
                    myIntent.putExtra("description", "Birds are a group of warm-blooded vertebrates constituting the class Aves /ˈeɪviːz/, characterised by feathers, toothless beaked jaws, the laying of hard-shelled eggs, a high metabolic rate, a four-chambered heart, and a strong yet lightweight skeleton. Birds live worldwide and range in size from the 5.5 cm (2.2 in) bee hummingbird to the 2.8 m (9 ft 2 in) ostrich. There are about ten thousand living species, more than half of which are passerine, or \"perching\" birds. Birds have wings whose development varies according to species; the only known groups without wings are the extinct moa and elephant birds. Wings, which evolved from forelimbs, gave birds the ability to fly, although further evolution has led to the loss of flight in some birds, including ratites, penguins, and diverse endemic island species. The digestive and respiratory systems of birds are also uniquely adapted for flight. Some bird species of aquatic environments, particularly seabirds and some waterbirds, have further evolved for swimming.");
                    myIntent.putExtra("Image", R.drawable.bird_large);
                    startActivity(myIntent);
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {}});
            return builder.create();
        }
    }
}