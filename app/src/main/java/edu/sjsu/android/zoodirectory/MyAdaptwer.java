package edu.sjsu.android.zoodirectory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyAdaptwer extends RecyclerView.Adapter<MyAdaptwer.ViewHolder>{
    private final List<Animal> animalList;
    private RecyclerViewClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView animalName;
        public View layout;
        public ImageView images;

        public ViewHolder(View v){
            super(v);
            layout = v;
            animalName = (TextView) v.findViewById(R.id.animalnames);
            images = (ImageView) v.findViewById(R.id.animalpics);
            v.setOnClickListener(this);
        }

        public TextView getAnimalName(){
            return animalName;
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }

    public MyAdaptwer(List<Animal> myDataset, RecyclerViewClickListener listener){
        animalList = myDataset;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyAdaptwer.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.row_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdaptwer.ViewHolder holder, int position) {
        holder.animalName.setText(animalList.get(position).getName());
        holder.images.setImageResource(animalList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return animalList.size();
    }

    public interface RecyclerViewClickListener{
        void onClick(View v, int position);
    }
}
